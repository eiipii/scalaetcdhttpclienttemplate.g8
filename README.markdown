A Giter8 template for testing Eiipii's Etcdhttpclient.


Template license
----------------
Written in 2016 by Antonio Matamoros Ochman antonio@eiipii.com and Paweł Sanjuan Szklarz paweld2@gmail.com.

To the extent possible under law, the author(s) have dedicated all copyright and related
and neighboring rights to this template to the public domain worldwide.
This template is distributed without any warranty. See <http://creativecommons.org/publicdomain/zero/1.0/>.

