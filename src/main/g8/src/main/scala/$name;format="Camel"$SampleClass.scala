package $package$.sampleClass

import com.eiipii.etcd.client.model.EtcdGetVersionResult
import com.eiipii.etcd.client.{EtcdClient, EtcdDSL}

import scala.concurrent.Future

class $name;format="Camel"$SampleClass (config: String) {

  val etcdClientConfig = EtcdDSL.config(config)
  val etcdCli = new EtcdClient(etcdClientConfig)

  def getEtcdVersion = {
    etcdCli.getVersion
  }

  def closeEtcdClient() = {
  etcdCli.close()
}
}
