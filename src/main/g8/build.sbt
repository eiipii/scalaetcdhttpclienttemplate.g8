import sbt.Keys.resolvers

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "$package$",
      scalaVersion := "2.11.8"
    )),
    name := "$name;format="Camel"$",
    libraryDependencies ++= Seq(
      "com.eiipii" %% "etcdhttpclient" % "0.1.0",
      "org.slf4j" % "log4j-over-slf4j" % "1.7.21",
      "org.scalatest" %% "scalatest" % "3.0.0" % Test,
      "com.whisk" %% "docker-testkit-scalatest" % "0.8.3" % Test
        exclude("org.slf4j", "slf4j-api"),
      "com.whisk" %% "docker-testkit-config" % "0.8.3" % Test
        exclude("org.slf4j", "slf4j-api")
    )
)